#!/usr/bin/env python3

import mechanize, requests, os

br = mechanize.Browser()
br.open('https://www.vagrantup.com/downloads.html')
for link in br.links(url_regex='_x86_64.deb'):
  deb_file = requests.get(link.url) # Get link to *.deb file
  open('vagrant.deb', 'wb').write(deb_file.content) # Download file
  command = os.popen('sudo dpkg -i vagrant.deb')
  print(command.read())
  print(command.close())
