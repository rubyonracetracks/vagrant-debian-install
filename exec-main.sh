#!/bin/bash

DIR_PWD=$PWD

echo '-------------------'
echo 'sudo apt-get update'
sudo apt-get update

# Install VirtualBox if it is not already installed
VBOX_MANAGE_VERSION=`vboxmanage --version | grep 'command not found'`
if [[ -z "$VBOX_MANAGE_VERSION" ]]
then
  bash install-virtualbox.sh
fi

echo '-------------------------------------------'
echo 'sudo apt-get install -y python3 python3-pip'
sudo apt-get install -y python3 python3-pip

echo '----------------------'
echo 'pip3 install mechanize'
pip3 install mechanize

echo '---------------------'
echo 'pip3 install requests'
pip3 install requests

python3 install-vagrant.py

echo '--------------------'
echo 'VBoxManage --version'
VBoxManage --version

echo '-----------------'
echo 'vagrant --version'
vagrant --version
